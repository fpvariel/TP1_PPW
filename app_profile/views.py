from django.shortcuts import render
from .models import profileuser

# Create your views here.
def index(request):
    try:
        profile = profileuser.objects.get(id=1)
        split = profile.expertise.split(',')
        response = {'title':'Profile', 'profile': profile, 'split':split}
        return render(request, 'profile.html', response)
    except profileuser.DoesNotExist:
        response = {'title':'Profile', 'profile': 'error', 'split':'error'}
        return render(request, 'profile.html', response)
