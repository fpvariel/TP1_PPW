from django.db import models

# Create your models here.
class profileuser(models.Model):
    photo = models.TextField(default="https://image.ibb.co/nOFEFb/glasses.png")
    name = models.CharField(max_length=30)
    birthday = models.DateField()
    gender = models.CharField(max_length=10)
    expertise = models.CharField(max_length=50)
    description = models.TextField()
    email = models.EmailField()
