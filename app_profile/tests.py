from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import profileuser
from datetime import datetime

class ProfileTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_Profile_Using_Index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_model_can_create_profile(self):
        new_activity = profileuser.objects.create(photo="https://image.ibb.co/nOFEFb/glasses.png", name = "Nadiem", birthday=datetime.now(), gender="male", expertise="programming", description="CEO Owjek", email="email@ui.ac.id" )
        counting_all_data = profileuser.objects.all().count()
        self.assertEqual(counting_all_data, 1)
