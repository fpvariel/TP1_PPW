from django import forms

class Post_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control'
    }
    post = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
