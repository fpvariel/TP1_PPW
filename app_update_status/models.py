from django.db import models

class Post(models.Model):
    post = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
    	return self.post
