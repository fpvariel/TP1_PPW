from django.urls import resolve
from django.test import TestCase
from django.test import Client
from .views import index, status_post
from .models import Post
from .forms import Post_Form
from app_profile.models import profileuser

class StatusUnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        response = Client().get('/update_status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update_status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_post(self):
        new_activity = Post.objects.create(post='mengerjakan tugas ppw')
        counting_all_available_todo = Post.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Post_Form(data={'post': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
        form.errors['post'],
        ["This field is required."]
        )
        
    def test_str_post(self):
        post = 'This is a test'
        new_post = Post.objects.create(post=post)
        self.assertEqual(str(new_post), post)

    def test_post_success_and_render_the_result(self):
        test = 'This is a test'
        response_post = Client().post('/update_status/add_post', {'post': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
