from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Post
from .forms import Post_Form
from app_profile.models import profileuser

response = {}

def index(request):
    # profile = profileuser.objects.get(pk=1)
    # profile.save()
    response['post_form'] = Post_Form
    # response['profile'] = profile
    html = 'app_update_status/app_update_status.html'
    post = Post.objects.all()
    response['post'] = post
    return render(request, html, response)
    
def status_post(request):
    form = Post_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
            response['post'] = request.POST['post']
            postingan = Post(post=response['post'])
            postingan.save()
            return HttpResponseRedirect('/update_status/')
