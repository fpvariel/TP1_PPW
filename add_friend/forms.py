from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'Please fill in your friend\'s herokuapp link',
    }
    name_attrs = {
        'class': 'form-control',
        'placeholder':'What is your friend\'s name?'
    }

    url_attrs = {
        'class': 'form-control',
        'placeholder':'What is your friend\'s herokuapp link?'
    }


    name = forms.CharField(label='Name', required=True, max_length=27, empty_value='Anonymous', 
        widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='URL', required=True, empty_value='Anonymous',
        widget=forms.URLInput(attrs=url_attrs))
    