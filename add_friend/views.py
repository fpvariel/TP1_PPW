from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

# Create your views here.
response = {}
def index(request):
	html = 'add_friend/add_friend.html'
	response['friend_form'] = Friend_Form
	friend = Friend.objects.all()
	response['friend'] = friend
	return render(request,html,response)
def add_friend(request):
	form = Friend_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['url'] = request.POST['url'] if request.POST['url'] != "" else "Anonymous"
		friend = Friend(name=form.cleaned_data['name'], url=form.cleaned_data['url'])
		friend.save()
	else:
		response['form'] = form
	html ='add_friend/add_friend.html'
	friend = Friend.objects.all()
	response['friend'] = friend
	return render(request,html,response)