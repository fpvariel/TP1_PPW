from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class statsUnitTest(TestCase):
	# def test_stats_is_exist(self):
	# 	response = Client().get('/stats/')
	# 	self.assertEqual(response.status_code,200)

	def test_stats_Using_Index_func(self):
		found = resolve('/stats/')
		self.assertEqual(found.func, index)


