from django.shortcuts import render
from app_profile.models import profileuser
from add_friend.models import Friend
from app_update_status.models import Post


def index(request):
    response = {}

    profile = profileuser.objects.get(pk=1) 
    friendCount = Friend.objects.all().count()
    feedCount = Post.objects.all().count()
    
    html = 'stats.html'
    response["profile"] = profile
    response["friends"] = friendCount
    response["feed"] = feedCount

    qs = Post.objects.order_by('-created_date')
    if(len(qs) > 0):
        latest_post = qs[0]
    else:
        latest_post = None

    response['latest_post'] = latest_post

    return render(request, html, response)
